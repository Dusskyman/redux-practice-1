import 'package:redux/redux.dart';
import 'package:redux_prectice/app_state.dart';
import 'package:redux_prectice/counter_page_state/counter_page_actions.dart';

class CounterPageSelectors {
  static int getValue(Store<AppState> store) {
    return store.state.counterPageState.counter;
  }

  static void Function() callIncrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(IncrementAction());
  }

  static void Function() callDecrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(DecrementAction());
  }

  static void Function() callZeroCounterFunction(Store<AppState> store) {
    return () => store.dispatch(ZeroAction());
  }
}
