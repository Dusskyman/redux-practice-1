import 'package:flutter/widgets.dart';
import 'package:redux/redux.dart';
import 'package:redux_prectice/app_state.dart';
import 'package:redux_prectice/counter_page_state/counter_page_selectors.dart';

class CounterPageViewModel {
  final int counter;
  final void Function() incrementCounter;
  final void Function() decrementCounter;
  final void Function() zeroCounter;

  CounterPageViewModel({
    @required this.counter,
    @required this.incrementCounter,
    @required this.decrementCounter,
    @required this.zeroCounter,
  });

  static CounterPageViewModel fromStore(Store<AppState> store) {
    return CounterPageViewModel(
      counter: CounterPageSelectors.getValue(store),
      incrementCounter:
          CounterPageSelectors.callIncrementCounterFunction(store),
      decrementCounter:
          CounterPageSelectors.callDecrementCounterFunction(store),
      zeroCounter: CounterPageSelectors.callZeroCounterFunction(store),
    );
  }
}
