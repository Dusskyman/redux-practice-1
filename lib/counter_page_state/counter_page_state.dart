import 'dart:collection';

import 'package:flutter/widgets.dart';
import 'package:redux_prectice/counter_page_state/counter_page_actions.dart';
import 'package:redux_prectice/reducer.dart';

class CounterPageState {
  final int counter;

  CounterPageState({
    @required this.counter,
  });
  factory CounterPageState.initial() {
    return CounterPageState(
      counter: 10,
    );
  }

  CounterPageState copyWith(int counter) {
    return CounterPageState(counter: counter ?? this.counter);
  }

  CounterPageState reducer(dynamic action) {
    return Reducer<CounterPageState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => _increment(),
        DecrementAction: (dynamic action) => _decrement(),
        ZeroAction: (dynamic action) => _toZero(),
      }),
    ).updateState(action, this);
  }

  CounterPageState _increment() {
    print(counter);
    return copyWith(counter + 1);
  }

  CounterPageState _decrement() {
    print(counter);
    return copyWith(counter - 1);
  }

  CounterPageState _toZero() {
    print(counter);
    return copyWith(0);
  }
}
