import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_prectice/app_state.dart';
import 'package:redux_prectice/appbar_state/appbar_vm.dart';
import 'package:redux_prectice/counter_page_state/counter_page_view_model.dart';

void main() {
  final Store store = Store<AppState>(
    AppState.getReducer,
    initialState: AppState.initial(),
  );
  runApp(MyApp(store));
}

class MyApp extends StatelessWidget {
  final Store store;
  MyApp(this.store);
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppBarViewModel>(
      converter: AppBarViewModel.fromStore,
      builder: (context, vm) => Scaffold(
        appBar: AppBar(
          backgroundColor: vm.color,
          leading: Switch(
            value: vm.switchVal,
            onChanged: (value) {
              vm.switchFunction(value);
              if (vm.switchVal) {
                return vm.setToBlue();
              }
              vm.setToRed();
            },
          ),
        ),
        body: StoreConnector<AppState, CounterPageViewModel>(
          converter: CounterPageViewModel.fromStore,
          builder: (context, vm) => Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    FloatingActionButton(
                      onPressed: () => vm.decrementCounter(),
                      child: Icon(Icons.remove),
                    ),
                    Text(
                      '${vm.counter}',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    FloatingActionButton(
                      onPressed: () {
                        vm.incrementCounter();
                      },
                      child: Icon(Icons.add),
                    ),
                  ],
                ),
                FloatingActionButton(
                  onPressed: () {
                    vm.zeroCounter();
                  },
                  child: Icon(Icons.exposure_zero),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
