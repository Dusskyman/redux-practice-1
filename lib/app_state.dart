import 'package:flutter/widgets.dart';
import 'package:redux_prectice/appbar_state/appbar_state.dart';
import 'package:redux_prectice/counter_page_state/counter_page_state.dart';

class AppState {
  CounterPageState counterPageState;
  AppBarState appBarState;
  AppState({@required this.counterPageState, @required this.appBarState});
  factory AppState.initial() {
    return AppState(
        counterPageState: CounterPageState.initial(),
        appBarState: AppBarState.initial());
  }
  static AppState getReducer(AppState state, dynamic action) {
    const String tag = '[appReducer]';
    print('$tag => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      counterPageState: state.counterPageState.reducer(action),
      appBarState: state.appBarState.reducer(action),
    );
  }
}
