import 'package:flutter/widgets.dart';
import 'package:redux/redux.dart';
import 'package:redux_prectice/app_state.dart';
import 'package:redux_prectice/appbar_state/appbar_actions.dart';

class AppbarSelectors {
  static bool getSwitchValue(Store<AppState> store) {
    return store.state.appBarState.switchVal;
  }

  static Color getColorValue(Store<AppState> store) {
    return store.state.appBarState.color;
  }

  static void Function() setToRedFunction(Store<AppState> store) {
    return () => store.dispatch(SetToRed());
  }

  static void Function() setToBlueFunction(Store<AppState> store) {
    return () => store.dispatch(SetToBlue());
  }

  static void Function(bool) switchBoolFunction(Store<AppState> store) {
    return (bool) => store.dispatch(SwitchBool());
  }
}
