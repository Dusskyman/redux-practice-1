import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:redux_prectice/appbar_state/appbar_actions.dart';

import '../reducer.dart';

class AppBarState {
  final bool switchVal;
  final Color color;

  AppBarState({
    @required this.switchVal,
    @required this.color,
  });
  factory AppBarState.initial() {
    return AppBarState(
      switchVal: false,
      color: Colors.red,
    );
  }

  AppBarState copyWith({bool switchVal, Color color}) {
    return AppBarState(
      switchVal: switchVal ?? this.switchVal,
      color: color ?? this.color,
    );
  }

  AppBarState reducer(dynamic action) {
    return Reducer<AppBarState>(
      actions: HashMap.from({
        SetToRed: (dynamic action) => _setToRed(),
        SetToBlue: (dynamic action) => _setToBlue(),
        SwitchBool: (dynamic action) => _switchBool(action),
      }),
    ).updateState(action, this);
  }

  AppBarState _setToRed() {
    return copyWith(color: Colors.red);
  }

  AppBarState _setToBlue() {
    return copyWith(color: Colors.blue);
  }

  AppBarState _switchBool(bool) {
    return copyWith(switchVal: !switchVal);
  }
}
