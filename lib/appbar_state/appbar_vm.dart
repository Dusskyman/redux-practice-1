import 'dart:ffi';

import 'package:flutter/widgets.dart';
import 'package:redux/redux.dart';
import 'package:redux_prectice/app_state.dart';
import 'package:redux_prectice/appbar_state/appbar_selector.dart';

class AppBarViewModel {
  final bool switchVal;
  final Color color;
  final void Function() setToRed;
  final void Function() setToBlue;
  final void Function(bool) switchFunction;

  AppBarViewModel({
    @required this.color,
    @required this.switchVal,
    @required this.setToRed,
    @required this.setToBlue,
    @required this.switchFunction,
  });

  static AppBarViewModel fromStore(Store<AppState> store) {
    return AppBarViewModel(
      switchVal: AppbarSelectors.getSwitchValue(store),
      color: AppbarSelectors.getColorValue(store),
      setToRed: AppbarSelectors.setToRedFunction(store),
      setToBlue: AppbarSelectors.setToBlueFunction(store),
      switchFunction: AppbarSelectors.switchBoolFunction(store),
    );
  }
}
